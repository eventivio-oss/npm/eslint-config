# Usage

Set registry:

`echo @eventivio-oss:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc`

Install:

`npm i -D @eventivio/eslint-config`

Add lint scripts:

```js
{
  "scripts": {
    "lint": "eslint src",
    "lint:fix": "eslint --fix src"
  },
}
```

Create .eslintrc.json with the following content:

```json
{
  "extends": [ "@eventivio-oss/eslint-config" ],
}
```

More config options [here](https://eslint.org/docs/user-guide/configuring/language-options).